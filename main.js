//id si butongöster olan butonu alıyoruz.
var buton=$('#butongöster');
// id si kutu olan kutu divini alıyoruz.
var getkutu=$('#kutu');
//buton tıklandığında bir yani bir olay eklemiş olduk
buton.on("click",function () {
  //kutuyu görünür yapacağız
  //getkutu.show();
  //hem gösterip hemde gizlemesi için her ikisini yapacak özellik olan toggle kullanabiliriz.
  //getkutu.toggle();
  //veya bunu if else yoluyla kutunun cssini kontrol ederek de yapabiliriz
  if(getkutu.css("display")=="none")//eğer görünür değilse
  {
    getkutu.show();
    //ve buton üzerindeki texti değiştireceğiz
    buton.text("Gizle");
  }
  else{// eğer görünürse
    getkutu.hide();//kutuyu gizle
    buton.text("Göster");
  }

});
